# Chat & User authentication Nuxt.js sample

This API uses :

- Koa2
- Nuxt.js
- ES6
- MongoDB/Mongoose
- Redis as a session storage
- Socket.io
- //TODO Mocha/Chai

### Under development, list of implemented things:

** Server side methods: **  

* ** Auth: **
	* Login
	* Logout
	* Singup
	
* ** Conversation: ** 
	* AddUserToConv
    * ConversationInfo
    * Create
    * DeleteUser
    * FindByEmail
    * GetAllMessages
    * GetConversations
    * SendMessage

** Client: **  

* ** Singup/login pages **

* ** Conversation: ** 
	* Find users
	* Live update messages, adding in new conversation and conversations preview via socket
	* Chat settings menu where chat master can kick users, everyone can add new users, shows a list of online participants
	* Messages infinite scroll
	* Emoji	


## Build Setup

### !!! Rename ./server/config.example.js and edit to your own taste

```sh
$ git clone https://bogatyr285@bitbucket.org/bogatyr285/koa2-auth-boilerplate.git && cd koa2-auth-boilerplate
```

* ** Default way:**

    * Launch MongoDB
	* Launch Redis

```sh
$ npm i & npm run dev
```

* ** Docker way:**

```sh
$ docker-compose up --build
```

### Commands
``` bash
# install dependencies
$ npm install

# generate server's API documentation(will be avaible at ./server/docs/index.html)
# or you can find screenshot with documentation page at './API documentation.png'
$ npm run docs

# serve with hot reload 
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate

```
