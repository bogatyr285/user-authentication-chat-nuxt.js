import Vue from 'vue'
import io from 'socket.io-client'
import { server as cfg } from '@/../server/config'

const socket = io(`http://${cfg.host==='0.0.0.0'?'localhost':cfg.host}:${cfg.port}`)

export default socket

//TODO dont know how to implement socket init auth here. If we do it here ouside function we cant reach token from store 
//In we insert init in export function will get WEBPACK_IMPORTED_MODULE_3__plugins_socket_io__.a.emit
// export default ({ store }) => {
//     console.log('SOCKET PLUGIN')
//     socket.on('connect', function () {
//         socket
//             .emit('authenticate', { token: store.state.userAuth.token })
//             .on('authenticated', function () {
//                 console.log('AUTH SUCCESSFUL')
//             })
//             .on('unauthorized', function (msg) {
//                 console.log("unauthorized: " + JSON.stringify(msg.data));
//                 throw new Error(msg.data.type);
//             })
//     });
//     return socket
// }