export default function({ $axios, app, store }) {
    $axios.onRequest(config => {
        if (store.state.authUser && store.state.authUser.token) {
            config.headers.common['Authorization'] = `JWT ${store.state.authUser.token}`
        }
    })
}