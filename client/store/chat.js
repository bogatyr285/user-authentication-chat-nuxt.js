import socket from '~/plugins/socket.io'

import Vue from 'vue';

export const state = () => ({
    authUser: null, //authUser from main store. will init in store/index.js
    curSelectedChat: null,//example "5bccb6fea3962b22b7479ced"
    conversations: [ //array contains conversations list with preview(author and last message)
        /*   
        example:
        {
              "title": "1337", "id": "5bccb6fea3962b22b7479ced",
              "lastMsg": {
                  "body": "qq\ng",
                  "author": { "displayName": "YobaBoba111", "id": "5bc8d075369fcd4978d55196" },
                  "createdAt": "2018-10-22T11:32:34.933Z",
                  "updatedAt": "2018-10-22T11:32:34.933Z"
              }
          } */
    ],
    messages: {//contains object with messages from conversation 
        curOffset: 0,
        reqAmount: 10,//how many messages we want to get from server
        store: []
        /*
           example 
        {
                "conversationId": "5bccb6fea3962b22b7479ced",
                "body": "0.5785631856186753",
                "author": { "displayName": "YobaBoba111", "id": "5bc8d075369fcd4978d55196" },
                "createdAt": "2018-10-21T17:27:27.026Z", "updatedAt": "2018-10-21T17:27:27.026Z"
            } */

    },
    onlineUsersInSelConv: []//contains info about online users in selected chat. add this var because we need to wait result from socket, can't do await in 
})
export const mutations = {
    SET_ACTIVE_CONVERSATION(state, id) {
        state.curSelectedChat = id;
    },
    SET_CONVERSATION_LIST(state, conversations) {
        state.conversations = conversations;
    },
    SET_CONV_MESSAGES(state, { convId, messages, isInitAdd/*bool */ }) {
        /*  if (!state.messages.store) {
             Vue.set(state.messages.store, convId, []);   //https://stackoverflow.com/questions/42807888/vuejs-and-vue-set-update-array
         } */
        messages = messages.reverse()
        if (isInitAdd) {
            state.messages.store.push(...messages);
        } else {
            state.messages.store.unshift(...messages);
        }
    },
    ADD_NEW_CONVERSATION(state, conversation) { //if someone make a new conversation with us we add to main store
        state.conversations.unshift(conversation)
    },
    PUSH_MESSAGE(state, { convId, message }) {
        /* if (!state.messages.store[convId]) {
            Vue.set(state.messages.store, convId, []);
        } */
        state.messages.store.push(message)
    },
    //change conversation preview(last message, author in conversations list) when we got new message
    CHANGE_CONV_PREVIEW(state, message) {
        const index = state.conversations.findIndex(conversation => { //find conversation index where we will update message 
            return conversation.id === message.conversationId
        })
        state.conversations[index].lastMsg = message //update message preview
        state.conversations.unshift(state.conversations[index]) //push to top
        state.conversations.splice(index + 1, 1) //remove this conversation(because we already added it to top)     
    },
    PUSH_ONLINE_USERS(state, onlineUsers) {
        state.onlineUsersInSelConv = onlineUsers
    },
    INCREASE_MSG_OFFSET(state, ) {
        state.messages.curOffset += state.messages.reqAmount
    },
    /*!!! TODO small bicycle with resetting whole store when selected chat changed.
    Need to redesign message store schema, probably store messages for each user
     Checking state in 'PUSH_MESSAGE' method commented because of mentioned reason
     */
    RESET_CUR_MSGS_STORE(state) {
        state.messages = {
            curOffset: 0,
            reqAmount: 10,
            store: []
        }
    },

}
export const actions = {
    //when user click on conversation we will get messages from conv
    async selectConversation({ state, commit, dispatch, error }, id) {
        if (state.curSelectedChat === id) {
            return
        }
        commit('SET_ACTIVE_CONVERSATION', id);
        commit('RESET_CUR_MSGS_STORE')
        try {
            await dispatch('fetchNewMsgs', state.curSelectedChat)
        } catch (error) {
            returnErr(error)
        }
    },
    async fetchNewMsgs({ state, commit }, convId) {
        try {
            const { data } = await this.$axios.get(`/conversation/${convId}?offset=${state.messages.curOffset}&count=${state.messages.reqAmount}`)
            if (state.messages.curOffset > data.response.total) {
                alert('conversation end')
                return
            }
            const isFirstFetch = state.messages.curOffset === 0 ? true : false;
            commit('SET_CONV_MESSAGES', { convId, messages: data.response.messages, isInitAdd: isFirstFetch })
            commit('INCREASE_MSG_OFFSET')

            return data.response
        } catch (error) {
            returnErr(error)
        }
    },
    //first load of chat page
    async initConversationList({ state, commit, error }) {
        try {
            let { data } = await this.$axios.get(`/conversations`);
            commit("SET_CONVERSATION_LIST", data.response.conversations);

        } catch (err) {
            returnErr(error)
        }
    },
    //init socket for all fetched conversations. We do this here because we cant do it in store/mutations because of SSR.
    // Call from pages/chat
    initSocketForConversations({ state }) {    
        socket.emit('init_user_info', state.authUser)
        state.conversations.forEach(conversation => {
            socket.emit('subcribe_on_conversation_updates', conversation.id)
        })
    },
    async sendMessage({ state, commit, dispatch, error }, message) {
        try {
            const { data } = await this.$axios.post(`/conversation/${state.curSelectedChat}/send`, { message });
            socket.emit('send_message', data.response.message)
            dispatch('pushMessage', { convId: state.curSelectedChat, message: data.response.message })
            return data.response;
        } catch (error) {
            returnErr(error)
        }
    },

    //Push message to messages and update conversation preview. call when we got new message from socket or send
    pushMessage({ commit }, { convId, message }) {
        console.log('push message to store', convId, message)
        commit('PUSH_MESSAGE', { convId, message })
        commit('CHANGE_CONV_PREVIEW', message)
    },
    //when we make dialog with someone we push created conv to store
    pushConversation({ commit }, conversation) {
        commit('ADD_NEW_CONVERSATION', conversation)
        socket.emit('subcribe_on_conversation_updates', conversation.id) //subcribe to created channel
    },
    //add users that is online in selected conv
    pushOnlineUsers({ commit }, onlineUsers) {
        commit('PUSH_ONLINE_USERS', onlineUsers)
    },
    //find user by email. return array with one finded user
    async findUser({ state, commit, error }, query) {
        try {
            const { data } = await this.$axios.get(`/conversation/findUser/${query}`);
            if (data.response.user) {
                return [data.response.user]
            }
            return []
        } catch (error) {
            returnErr(error)
        }
    },
    //make new conversation with selected user
    async makeDialog({ commit, dispatch }, { recipientId, message, title }) {
        try {
            const { data } = await this.$axios.post(`/conversation/new/${recipientId}`, { message, title });
            console.log('make dialog', data.response)
            //TODO bug here. Notification about new user will receive only new user. Because we emit only 'new_conversation', need redesign API
            socket.emit('new_conversation', recipientId, data.response.conversation) //notify recipientId about he was added
            dispatch('pushConversation', data.response.conversation) //add conv to our store and init socket 
        } catch (error) {
            returnErr(error)
        }
    },
    //retreive info about defined conversation. uses in conversation settings menu
    async getConvInfo({ dispatch }, { convId }) {
        try {         
            const { data } = await this.$axios.get(`/conversation/info/${convId}`)
            socket.emit('get_online_in_conv', convId)

            return data.response
        } catch (error) {
            returnErr(error)
        }
    },
    //delete user from defined conversation
    async deleteUserFromConv({ dispatch }, { convId, participant }) {
        try {
            const { data } = await this.$axios.delete(`/conversation/${convId}/${participant.id}`)
            console.log('deleteUserFromConv', data.response)
            await dispatch('sendMessage', `${participant.displayName} was removed`)
            socket.emit('delete_user', convId, participant.id)
        } catch (error) {
            returnErr(error)
        }
    },
    //add new user to existing conv
    async addUserToConv({ dispatch, state, commit }, { convId, user }) {
        try {
            const { data } = await this.$axios.post(`/conversation/${convId}/add/${user.id}`)
            console.log('addUserToConv', data.response)
            commit('PUSH_MESSAGE', { convId, message: data.response.conversation.lastMsg })
            socket.emit('new_conversation', user.id, data.response.conversation) //notify recipientId about he was added   
        } catch (error) {
            returnErr(error)
        }
    }
}

export const getters = {
    getCurSelectedChat: state => { return state.curSelectedChat },
    getConvsList: (state) => { return state.conversations },
    getChatMsgs: (state) => { return convId => state.messages.store },
    getCurAuthUser: (state) => { return state.authUser },
    getCurConvOnlineUsers: (state) => { return state.onlineUsersInSelConv }
}

function returnErr(error) {
    if (error.response) {
        $nuxt.error({
            statusCode: error.response.status,
            message: error.response.data.error.message
        });
        console.log('req err', error.response)
    } else {
        $nuxt.error({
            statusCode: error.statusCode,
            message: error.message
        });
        console.log('client err', error)
    }
}
