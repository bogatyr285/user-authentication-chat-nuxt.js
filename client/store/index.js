import socket from '~/plugins/socket.io'

export const state = () => ({
    authUser: null,
})

export const mutations = {
    SET_USER(state, user) {
        state.authUser = user
        if (user && user.token) {
            this.$axios.setToken(user.token, 'JWT') // Setup axios
            state.chat.authUser = user //add userInfo to chat store   
        }
    },
}

export const actions = {
    // nuxtServerInit is called by Nuxt.js before server-rendering every page
    nuxtServerInit({ commit, store }, { req }) {
        if (req.session && req.session.authUser) {
            // console.log('store serv init', req.session.authUser)
            commit('SET_USER', req.session.authUser)
        }
        
    },
    async singup({ commit }, { displayName, email, password }) {
        try {
            const { data } = await this.$axios.post('/auth/singup', { displayName, email, password })
        } catch (error) {
            if (error.response && error.response.data && error.response.data.error && error.response.data.error.message) {
                throw new Error(error.response.data.error.message)
            }
            throw error
        }
    },
    async login({ commit }, { email, password }) {
        try {
            const { data } = await this.$axios.post('/auth/login', { email, password })
            console.log('login got', data)
            commit('SET_USER', data.response.user)
        } catch (error) {
            commit('SET_USER', null)
            if (error.response && error.response.status === 401) {
                throw new Error(error.response.data.error.message)
            }
            throw error
        }
    },
    async logout({ commit, state }) {
        this.$router.push("/");
        await this.$axios.post('/auth/logout')
        socket.emit('logout', state.authUser.id)
        this.$axios.setToken(null, 'JWT') // delete axious token
        commit('SET_USER', null)
    },
}