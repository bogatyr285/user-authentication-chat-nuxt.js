FROM node:10 
#mhart/alpine-node:10
#!!! DO NOT USE ALPINE IMAGES BECAUSE OF INCOPATIBLE WITH bcrypt
RUN mkdir -p /projects/chat
WORKDIR /projects/chat

COPY package.json /projects/chat/package.json
COPY package-lock.json /projects/chat/package-lock.json
# COPY ./package* ./
RUN cd /projects/chat \
	#&& apk --no-cache add --virtual builds-deps build-base python \
	&& echo ">> Installing NPM packages" \
	&& npm i 
COPY . /projects/chat

RUN echo ">> Build project" \
	&& npm run build