const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const pkg = require('./package')
const { server: serverCfg } = require('./server/config')
module.exports = {
    mode: 'universal',
    srcDir: 'client/',
    /*
     ** Headers of the page
     */
    head: {
        title: pkg.name,
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: pkg.description }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
     ** Customize the progress-bar color
     */
    loading: {
        color: 'green',
        height: '10px'
    },

    /*
     ** Global CSS
     */
    css: [],

    /*
     ** Plugins to load before mounting the App
     */
    plugins: [
        '~/plugins/vuelidate',
        '~/plugins/axios',
        { src: '~/plugins/socket.io.js', ssr: false }
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        '@nuxtjs/axios',
        // Doc: https://bootstrap-vue.js.org/docs/
        'bootstrap-vue/nuxt',
        //https://www.npmjs.com/package/nuxt-fontawesome
        ['nuxt-fontawesome', {
            component: 'fa',
            imports: [
                //import whole set
                {
                    set: '@fortawesome/free-solid-svg-icons',
                    icons: ['fas']
                },
            ]
        }]
    ],
    /*
     ** Axios module configuration
     */
    axios: {
        // See https://github.com/nuxt-community/axios-module#options
        baseURL: `http://${serverCfg.host === '0.0.0.0' ? 'localhost' : serverCfg.host}:${serverCfg.port}/api`,
        // baseURL:`${location.protocol}//${location.hostname}:${serverCfg.port}/api`
        // prefix: '/api',
        //port: '8080',
        // host: 'localhost'       
    },

    /*
     ** Build configuration
     */
    build: {
        vendor: ['vuelidate'],
        optimization: {
            minimizer: [new UglifyJsPlugin({
                uglifyOptions: {
                  warnings: false,
                  parse: {},
                  compress: {
                    drop_debugger: false,
                    unused: false,
                    dead_code: true,
                    typeofs: false,
                  },
                  mangle: true, // Note `mangle.properties` is `false` by default.
                  output: null,
                  toplevel: false,
                  nameCache: null,
                  ie8: false,
                  keep_fnames: false,
                }
              })]
        },
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            // Run ESLint on save
            if (ctx.isDev && ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    }
}