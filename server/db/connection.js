const mongoose = require('mongoose')
const bluebird = require('bluebird')
mongoose.Promise = bluebird

const { mongo: mongoCfg } = require('../config')
const Logger = require('../logger')
const log = Logger(module);

const mongoUrl = `mongodb://${mongoCfg.host}:${mongoCfg.port}/${mongoCfg.collectionName}`

bluebird.promisifyAll(mongoose)

const setup = () => {
    return new Promise((resolve, reject) => {
        mongoCfg.debug ? mongoose.set('debug', true) : '';
        mongoose.connect(mongoUrl, (err) => {
            if (err) {
                log.error(`Mongo connection error: ${err.message}`, )
                throw { name: 'MONGO_CONN_ERR', message: err.message }
            }
            log.info(`Mongo connected to ${mongoUrl}`);
            return resolve()
        });
    })
}

module.exports = setup