const mongoose = require('mongoose');

const Logger = require('../../logger');
const log = Logger(module);

const ConversationSchema = new mongoose.Schema({
    title: {
        type: String
    },
    master:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    participants: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
}, {
    timestamps: true,
    toJSON: {
        transform: (doc, ret, options) => {
            delete ret.__v;
            ret.id = ret._id; //change _id to id
            delete ret._id;
        }
    }
});

module.exports = mongoose.model('Conversation', ConversationSchema);