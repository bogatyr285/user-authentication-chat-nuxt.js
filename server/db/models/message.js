const mongoose = require('mongoose');

const Logger = require('../../logger');
const log = Logger(module);

const MessageSchema = new mongoose.Schema({
    conversationId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Conversation',
        required: true,
    },
    body: {
        type: String,
        required: true,
        maxlength: 1000, //max message size limit
    },
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
}, {
        timestamps: true,
        toJSON: {
            transform: (doc, ret, options) => {
                delete ret._id;
                delete ret.__v;
            }
        }
    });

module.exports = mongoose.model('Message', MessageSchema);