const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Logger = require('../../logger');
const log = Logger(module);

const SALT_ROUNDS = 10;

const UserSchema = new mongoose.Schema({
    displayName: {
        type: String,
        required: `Username didn't specified`,
        match: [/^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я0-9-_\.]{5,20}$/, 'Please provide a valid username. Requirements: begin with letter, length 5-20 letters, letter and numbers only allowed']
    },
    email: {
        type: String,
        required: `Email didn't specified`,
        unique: true,
        lowercase: true,
        match: [/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/,
            `Please provide a valid email address`
        ]
    },
    password: {
        type: String,
        required: true
    },
    salt: String,
    // isVerified: {
    //     type: Boolean,
    //     default: false
    // },
    // verifyId: {
    //     type: String,
    //     required: true
    // },
    passwordReset: {
        token: String,
        date: Date,
    }
}, {
    timestamps: true,
    toJSON: {
        transform: (doc, ret, options) => {
            ret.id = ret._id; //change _id to id
            delete ret._id;
            delete ret.password;
            delete ret.salt;
            delete ret.__v;
            delete ret.verifyId;
            delete ret.passwordReset;
        }
    }
});

UserSchema.pre('save', async function preSave(next) {
    let user = this;
    // only if password was modified
    if (!user.isModified('password')) {
        next();
    }

    try {
        const { password, salt } = await cryptPasswd(user.password, SALT_ROUNDS);
        user.salt = salt;
        user.password = password
        next();
    } catch (error) {
        log.error(`${error.name} ${error.message}`);
        next(error);
    }
});
UserSchema.pre('updateOne', async function preSave(next) {
    const user = this;
    const passwordSrc = user.getUpdate().password;

    if (!passwordSrc) { //if password wasn't modified we skip this process      
        return next();
    }

    try {
        const { password, salt } = await cryptPasswd(passwordSrc, SALT_ROUNDS);
        user.getUpdate().password = password;
        user.getUpdate().salt = salt;
        next();
    } catch (error) {
        log.error(`${error.name} ${error.message}`);
        next(error);
    }
});

UserSchema.methods.hasValidPassword = function(testPassword) {
    if (!testPassword || !this.password) return false;
    return bcrypt.compareSync(testPassword, this.password)
};


async function cryptPasswd(passwd, rounds = 10) {
    const salt = await bcrypt.genSaltSync(rounds); //default 10 rounds  
    const password = await bcrypt.hashSync(passwd, salt);
    return { password, salt }
}

module.exports = mongoose.model('User', UserSchema);