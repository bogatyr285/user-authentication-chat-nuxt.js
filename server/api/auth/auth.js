const passport = require('koa-passport')
const { User } = require('../../db/models')

const { LocalStrategy, JwtStrategy } = require('./strategies')
const Logger = require('../../logger')
const log = Logger(module);

passport.use(LocalStrategy)
passport.use(JwtStrategy)
passport.serializeUser(function(user, done) {
    //log.debug(`Serializing: ${JSON.stringify(user)}`);
    done(null, user.id);
})
passport.deserializeUser(async(id, done) => {
    //log.debug(`Deserializing: ${id}`);
    let user = await User.findById(id);
    if (!user) {
        return done({ message: `User ID does not exist. Id:${id}` });
    }
    return done(null, user)
})

module.exports = passport;