const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const { jwt: jwtCfg } = require('../../../config')

const { User } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme("jwt"),
    secretOrKey: jwtCfg.secret,
    issuer: jwtCfg.issuer,
};

//----------Passport JWT Strategy--------//

// Expect JWT in the http header

module.exports = new JwtStrategy(jwtOptions, async function(payload, done) {
    try {
        log.debug(`Payload: ${JSON.stringify(payload)}`)

        let user = await User.findById(payload.id);
        if (!user) {
            log.debug(`id not exist. No such user`)
            return done({ message: 'No such user' }, false);
        }
        return done(null, user)
    } catch (error) {
        log.error(`Fetch from DB err: ${error.name} ${error.message} ${error.stack}`)
        return done({ message: 'Internal DB err' }, false)
    }
})