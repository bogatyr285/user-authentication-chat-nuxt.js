const LocalStrategy = require('passport-local').Strategy;

const { User } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);

module.exports = new LocalStrategy({ usernameField: 'email', passwordField: 'password' }, async(email, password, done) => {
    try {
        log.debug(`Email:${email} Pswd:${password}`)
        let user = await User.findOne({ email });
        if (!user) {
            log.debug(`return email not exist`)
            return done({ message: 'Email does not exist' }, false);
        }
        let hasValidPassword = await user.hasValidPassword(password)
        if (!hasValidPassword) {
            return done({ message: 'Wrong password' }, false);
        }

        return done(null, user)
    } catch (error) {
        log.error(`Fetch from DB err: ${error.name} ${error.message}  ${error.stack}`)
        return done({ message: 'Internal DB err' }, false)
    }
})