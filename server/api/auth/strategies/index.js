module.exports = {
    LocalStrategy: require('./local'),
    JwtStrategy: require('./jwt')
}