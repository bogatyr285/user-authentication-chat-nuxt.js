const socketIO = require('socket.io')
const Logger = require('../logger');
const log = Logger(module);
const http = require('http')
const socketioJwt = require('socketio-jwt')


let socketList = {};
//TODO smth went wrong with socket auth. see client/plugins/socket
const socketInit = async (server) => {
    const io = socketIO(server)

    io.on('connection', /* socketioJwt.authorize({
        secret: 'some_secretData_2W3D>@Yu5>bPc&M%#PCsVH)WX94bZh6u}',
        timeout: 15000
    })).on('authenticated',  */function (socket) {

       // console.log('socket. decoded token', socket.decoded_token);

        socket
            .on('logout', (userId) => {
                //leave from all subscribed channels when user logout
                log.info(`${userId} diconnect`)
                socket.emit('user_offline', userId)

                /* const rooms = io.sockets.adapter.rooms;
                for (let room in rooms)
                    if (!rooms[room].hasOwnProperty(room)) {
                        socket.leave(room)
                    }
    */
            })
            .on('init_user_info', (user) => { //all users join group equals own mongoID. This'll help to send private notification
                log.info(`#init_user_info ${JSON.stringify(user)}`)
                /*TODO now we have doubled rooms. First one is rooms with 'sokcetId', another one is our created room with mongoId. 
                    Need to find away how to get rid of default socket rooms
                        //  socket.id=user.id  //doent work
                    */
                socketList[socket.id] = user.id; //socketId-mongoId object. need for deleting user and get online status
                socket.join(user.id)
            })
            .on('subcribe_on_conversation_updates', (conversationId) => { //Init method. when user is online he subscribes to all of their conversations
                log.info(`#subcribe_on_conversation_updates ${socketList[socket.id]} join ${conversationId}`)
                socket.join(conversationId)
                //  socket.broadcast.to(conversationId).emit('user_online', userId)
            })

            //notify new user by mongoId that he was added to chat
            .on('new_conversation', (recipientId, message) => {
                log.info(`#new_conversation ${socketList[socket.id]} send new conv to: ${recipientId}. Init message: ${JSON.stringify(message)}`)
                socket.broadcast.to(recipientId).emit('new_conversation', message)
            })
            .on('send_message', (message) => {
                log.info(`#send_message ${socketList[socket.id]} send new msg ${message.body}`)
                socket.broadcast.to(message.conversationId).emit('new_message', message)
            })
            .on('delete_user', (convId, userId) => {
                log.info(`#delete_user ${socket.id} delete ${userId} from ${convId}`)
                let socketIdToDelete = null
                for (let socketId of socketList) {
                    if (socketList[socketId] === userId) {
                        socketIdToDelete = socketId
                    }
                }
                delete socket.adapter.rooms[convId].sockets[socketIdToDelete]
                delete socketList[socket.id]
            })
            .on('get_online_in_conv', (convId) => {
                const connectedUsers = io.sockets.adapter.rooms[convId].sockets;
                const onlineUsersIds = []
                for (let socketId in connectedUsers) {
                    onlineUsersIds.push(socketList[socketId]);
                }
                socket.emit('online_in_conv', onlineUsersIds)   // or like this io.sockets.connected[socket.id].emit('online_in_conv', 'qq')
            })
    });
}


module.exports = socketInit

//add Socket.io middleware to parse Koa-session cookie
// io.use((socket, next) => {
//     let error = null;
//     try {
//         // console.log(socket.handshake)
//         // create a new (fake) Koa context to decrypt the session cookie
//         let ctx = app.createContext(socket.request, new http.OutgoingMessage());
//         socket.session = ctx.session;
//         console.log('socket sess', ctx)
//     } catch (err) {
//         error = err;
//     }
//     return next(error);
// });