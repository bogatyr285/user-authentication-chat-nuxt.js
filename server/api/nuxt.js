const { Nuxt, Builder } = require('nuxt')
const nuxtInit = async() => {
    // Import and Set Nuxt.js options
    let config = require('../../nuxt.config.js')
    config.dev = !(process.env.NODE_ENV === 'production')
        // Instantiate nuxt.js
    const nuxt = new Nuxt(config)

    // Build in development
    if (config.dev) {
        const builder = new Builder(nuxt)
        await builder.build()
    }
    return nuxt
}
module.exports = nuxtInit