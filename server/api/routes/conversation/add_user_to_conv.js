const mongoose = require('mongoose');
const { Conversation, Message,User } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module)

//add requested user to existing conversation
module.exports = addUserToConvHandler = async (ctx, next) => {
    const { conversationId, user } = await parseReq(ctx)
        .catch(err => {
            ctx.throw(err.code, err.message)
        })
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(Object.assign(ctx.params,ctx.request.body))}`);

    const updatedConv = await Conversation.findByIdAndUpdate(conversationId,
        { $push: { participants: [user.id] } },
    )
    const newMsg = await Message.create({
        conversationId: updatedConv.id,
        body: `${user.displayName} was added by ${ctx.state.user.displayName}`,
        author: ctx.state.user.id
    })
    const message = await Message.findById(newMsg.id) //append to message author's info
        .populate({ //fetch displayName from User model
            path: 'author',
            select: 'displayName'
        })


    log.info(`#${ctx.state.reqId}. User added to conv`)
    ctx.status = 200
    return ctx.body = {
        status: 'ok',
        response: {
            conversation: {
                title: updatedConv.title,
                id: updatedConv.id,
                lastMsg: message
            }
        }
    }
}

async function parseReq(ctx) {
    const senderId = ctx.state.user.id;
    const { id: conversationId, userId } = ctx.params;
    const isValidConvId = mongoose.Types.ObjectId.isValid(conversationId)
    if (!isValidConvId) {
        throw { code: 422, message: `Invalid conversation ID` } //422 "unprocessable entity"
    }

    const requestedConv = await Conversation.findOne({
        _id: conversationId,
        participants: {
            $all: [senderId]
        }
    })

    if (!requestedConv) {
        throw { code: 422, message: `You are not a participant of ${conversationId} conversation` }
    }

    const userAlreadyInConv = requestedConv.participants.indexOf(userId) !== -1;
    if (userAlreadyInConv) {
        throw { code: 422, message: `User ${userId} already in ${conversationId} conversation` }
    }

    const userToAdd = await User.findById(userId)
    if (!userToAdd) {
        throw { code: 422, message: `User ID not found` }
    }

    return { conversationId, user: userToAdd }
}