const mongoose = require('mongoose');
const { Conversation } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);
//delete user from defined conv
module.exports = deleteUserHandler = async (ctx, next) => {
    const { conversationId, userId } = await parseReq(ctx)
        .catch(err => {
            ctx.throw(err.code, err.message)
        })

    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl}.  Params:${JSON.stringify(Object.assign(ctx.params,ctx.request.body))}`);

    await Conversation.findByIdAndUpdate(conversationId,
        { $pullAll: { participants: [userId] } }, //don't know why $pull doenst work, only pullAll
    )

    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: {
            deleted: userId
        }
    };
}

async function parseReq(ctx) {
    const { id: conversationId, userId } = ctx.params;
    const isValidConvId = mongoose.Types.ObjectId.isValid(conversationId)
    if (!isValidConvId) {
        throw { code: 400, message: 'Unexisting conversation ID' }
    }
    const isValidUserId = mongoose.Types.ObjectId.isValid(userId)
    if (!isValidUserId) {
        throw { code: 400, message: 'Unexisting user ID' }
    }
    //verify that the user is in this group and sender is master of group
    const authorId = ctx.state.user.id;
    const convExist = await Conversation.find({
        _id: conversationId,
        master: authorId,
        participants: {
            $all: [userId]
        },
    })
    if (convExist.length === 0) {
        throw { code: 422, message: 'You are not master of chat or unexisting user ID' }
    }

    return { conversationId, userId }
}