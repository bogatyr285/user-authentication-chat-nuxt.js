const { authenticated_jwt } = require('../../middlewares')
const newConversationHandler = require('./new')
const findByEmailHandler = require('./find_by_email')
const getConversationsHandler = require('./get_conversations')
const getConversationHandler = require('./get_conversation')
const sendMsgHandler = require('./send_msg')
const getConversationInfoHandler = require('./info')
const deleteUserHandler = require('./delete_user')
const addUserToConvHandler = require('./add_user_to_conv')

//TODO offsets to conversations
module.exports = (router) => {
    /**
     * @api {get} /api/conversation/findUser/:email FindByEmail
     * @apiDescription Find user by email
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName FindByEmail
     * @apiGroup Conversation
     *
     * @apiExample Example usage: 
     * curl -X GET   http://localhost:8000/api/conversation/findUser/qq@qq.qq   -H 'Authorization: JWT ...' 
     *
     * @apiParam {String} email Email.
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {Object}   response.user     Info about user (id, displayname)
     *   
     *
     * @apiSuccessExample {json} UserFound:
     *     HTTP/1.1 200 OK
     *       {
     *       "status": "ok",
     *       "response": {
     *           "user": {
     *               "id": "5b1f6bf97b96192b9e1cc4a3",
     *               "displayName": "YobaBoba111",
     *               "email": "qq@qq.qq",
     *               "createdAt": "2018-06-12T06:45:13.237Z",
     *               "updatedAt": "2018-06-12T06:45:13.237Z"
     *           }
     *       }
     *      }
     * @apiSuccessExample {json} UserNotFound:
     *     HTTP/1.1 200 OK
     *       {
     *       "status": "ok",
     *       "response": {
     *           "user": null
     *       }
     *      }   
     *    
     */
    router.get('/conversation/findUser/:email', authenticated_jwt, findByEmailHandler)

    /**
     * @api {post} /api/conversation/new/:recipient Create
     * @apiDescription Create new conv with requested user
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName NewConversation
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X POST   http://localhost:8000/api/conversation/new/5b1f69b20c92e72b91675ccf   -H 'Authorization: JWT ...'   -H 'Content-Type: application/x-www-form-urlencoded'   -d message=HELOL
     *
     * @apiParam {String} recipient RecipientID.
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {Object}   response.conversation  Info about created conv
     *   
     *
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 201 CREATED
     *       {
     *           "status": "ok",
     *           "response": {
     *               "conversation": {
     *                   "title": "Yoba222",
     *                   "id": "5b4b49828a9134036cf47d15",
     *                   "lastMsg": {
     *                       "conversationId": "5b4b49828a9134036cf47d15",
     *                       "body": "HELOL",
     *                       "author": {
     *                           "displayName": "Yoba111",
     *                           "id": "5b3349f7ac63a90c7779489d"
     *                       },
     *                       "createdAt": "2018-07-15T13:17:54.272Z",
     *                       "updatedAt": "2018-07-15T13:17:54.272Z"
     *                   }
     *               }
     *           }
     *       }
     * @apiError (Error 422) UnprocessableEntity Please choose a valid recipient for your message
     * @apiError (Error 422) UnprocessableEntity_ Please enter a message
     * @apiError (Error 422) UnprocessableEntity__ Please enter a title
     * @apiError (Error 422) UnprocessableEntity___ You already have a conversation with this user
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *           "status": "error",
     *           "error": {       
     *                 "name": "UnprocessableEntityError",
     *                   "message": "You already have a conversation with this user",
     *           }
     *       } 
     *    
     */
    router.post('/conversation/new/:recipient', authenticated_jwt, newConversationHandler)

    /**
     * @api {get} /api/conversations GetConversations
     * @apiDescription Get snippets with conversationsID and last message in this conversation
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName GetConvs
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X GET  http://localhost:8000/api/conversations   -H 'Authorization: JWT ...'
     *
     *   
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {Number}   response.total    Total number of conversations
     * @apiSuccess {Array}    response.conversations     Array with conversations
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 200 OK
     *       {
     *           "status": "ok",
     *           "response": {
     *               "total": 1,
     *               "conversations": [
     *                   {
     *                       "title": "Yoba222",
     *                       "id": "5b4b49828a9134036cf47d15",
     *                       "lastMsg": {
     *                           "body": "HELOL",
     *                           "author": {
     *                               "displayName": "Yoba111",
     *                               "id": "5b3349f7ac63a90c7779489d"
     *                           },
     *                           "createdAt": "2018-07-15T13:17:54.272Z",
     *                           "updatedAt": "2018-07-15T13:17:54.272Z"
     *                       }
     *                   }
     *               ]
     *           }
     *       }
     *    
     */
    router.get('/conversations', authenticated_jwt, getConversationsHandler)

    /**
     * @api {get} /api/conversation/:id GetAllMessages
     * @apiDescription Get all messages in this conversation
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName GetAllMsg
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X GET http://localhost:8000/api/conversation/5b240e2284f6d24b356420a1?offset=10&count=2
     *
     * @apiParam {String} id ConversationID.
     * @apiParam {QueryString} offset offset
     * @apiParam {QueryString} limit limit
     * 
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {Number}   response.total    Total number of messages
     * @apiSuccess {Number}   response.offset   Requested offset
     * @apiSuccess {Number}   response.count    Requested count
     * @apiSuccess {Array}    response.messages     Array with conversation messages
     * @apiSuccess {Object}   response.messages     Conversation info
     * @apiSuccess {String}   response.messages.conversationId     Conversation ID
     * @apiSuccess {String}   response.messagfes.body    Text message
     * @apiSuccess {Object}   response.messages.author    Author
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 200 OK
     *       {
     *           "status": "ok",
     *           "response": {
     *               "total": 2,
     *               "offset": 10,
     *               "count": 2,    
     *               "messages": [
     *                   {
     *                       "conversationId": "5b240e2284f6d24b356420a1",
     *                       "body": "HELOL 111",
     *                       "author": {
     *                           "id": "5b1f6bf97b96192b9e1cc4a3",
     *                           "displayName": "1234"
     *                       },
     *                       "createdAt": "2018-06-17T07:45:17.254Z",
     *                       "updatedAt": "2018-06-17T07:45:17.254Z"
     *                   },
     *                   {
     *                       "conversationId": "5b240e2284f6d24b356420a1",
     *                       "body": "HELOL",
     *                       "author": {
     *                           "id": "5b1f6bf97b96192b9e1cc4a3",
     *                           "displayName": "1234"
     *                       },
     *                       "createdAt": "2018-06-15T19:06:10.365Z",
     *                       "updatedAt": "2018-06-15T19:06:10.365Z"
     *                   }
     *               ]
     *           }
     *       } 
     * @apiError (Error 400) BadRequest Invalid conversation id
     * @apiError (Error 400) BadRequest Invalid offset
     * @apiError (Error 400) BadRequest Invalid count
     * @apiError (Error 422) UnprocessableEntity Unexisting conversation ID
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *           "status": "error",
     *           "error": {       
     *                 "name": "BadRequestError",
     *                 "message": "Bad conversation id",
     *           }
     *       } 
     *    
     */
    router.get('/conversation/:id', authenticated_jwt, getConversationHandler)

    /**
     * @api {post} /api/conversation/:id/send SendMessage
     * @apiDescription SendMessage to user
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName SendMessage
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X POST   http://localhost:8000/api/conversation/5b240e2284f6d24b356420a1/send   -H 'Authorization: JWT ...'   -H 'Content-Type: application/x-www-form-urlencoded'   -d message=HELLOL%20111
     *
     * @apiParam {String} id RecipientID.
     * @apiParam {String} message Message. Max length 1500
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {String}   response.message  Message and author info
     *   
     *
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 200 OK
     *       {
     *       "status": "ok",
     *       "response": {
     *            "message": {
     *              "conversationId":"5b240e2284f6d24b356420a1"
     *              "body":"HELOL 111"
     *               "author": {
     *                   "displayName": "1234",
     *                   "id": "5b1f6bf97b96192b9e1cc4a3"
     *               },
     *               "createdAt": "2018-06-27T07:43:10.165Z",
     *               "updatedAt": "2018-06-27T07:43:10.165Z"        
     *          }
     *       }
     *      }
     * @apiError (Error 422) UnprocessableEntity Unexisting conversation ID
     * @apiError (Error 400) BadRequest Unspecified 'msg' field
     * @apiError (Error 400) BadRequest Invalid conversation id
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *           "status": "error",
     *           "error": {       
     *                 "name": "UnprocessableEntityError",
     *                 "message": "Unexisting conversation ID",
     *           }
     *       } 
     *    
     */
    router.post('/conversation/:id/send', authenticated_jwt, sendMsgHandler) //send msg to conv


  /**
     * @api {get} /api/conversation/info/:id ConversationInfo
     * @apiDescription Fetch info about defined conversation
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName ConversationInfo
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X GET   http://localhost:8000/api/conversation/info/5b240e2284f6d24b356420a1   -H 'Authorization: JWT ...'
     *
     * @apiParam {String} id ConversationID.
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {String}   response.conversation  Conversation info
     *   
     *
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 200 OK
     *  {  
     *      "status":"ok",
     *      "response":{  
     *          "conversation":{
     *              "id":"5bcb2f57a8921954507c6b51",  
     *               "participants": [
     *                  {
     *                      "displayName": "YobaBoba111",
     *                      "id": "5bc8d075369fcd4978d55196"
     *                  },
     *                  {
     *                      "displayName": "YobaBoba222",
     *                      "id": "5bc8d07d369fcd4978d55197"
     *                  }
     *              ],              
     *              "master":{  
     *                  "displayName":"YobaBoba111",
     *                  "id":"5bc8d075369fcd4978d55196"
     *              },
     *              "title":"YOBA2CONV",
     *              "createdAt":"2018-10-20T13:36:23.611Z",
     *              "updatedAt":"2018-10-20T13:36:23.611Z"
     *          }
     *      }
     *  }
     * @apiError (Error 422) UnprocessableEntity Unexisting conversation ID
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *           "status": "error",
     *           "error": {       
     *                 "name": "UnprocessableEntityError",
     *                 "message": "Unexisting conversation ID",
     *           }
     *       } 
     *    
     */
    router.get('/conversation/info/:id', authenticated_jwt , getConversationInfoHandler)

    /**
     * @api {delete} /conversation/:id/:userId DeleteUser
     * @apiDescription Delete user from conversation
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName DeleteUser
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X DELETE   http://localhost:8080/api/conversation/5bcc4483eba8e7562e9f54f7/5bc8d075369fcd4978d55196  -H 'Authorization: JWT ...' 
     *
     * @apiParam {String} id ConversationID.
     * @apiParam {String} id UserID.
     * 
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {String}   response.deleted  UserID that was requested 
     *   
     *
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 200 OK
     *  {  
     *      "status":"ok",
     *      "response":{
     *          "deleted":"5bc8d075369fcd4978d55196"  
     *      }
     *  }
     * @apiError (Error 422) UnprocessableEntity Unexisting conversation ID
     * @apiError (Error 422) UnprocessableEntity_ Unexisting Unexisting user ID
     * @apiError (Error 422) UnprocessableEntity__ You are not master of chat or unexisting user ID
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *           "status": "error",
     *           "error": {       
     *                 "name": "UnprocessableEntityError",
     *                 "message": "Unexisting conversation ID",
     *           }
     *       } 
     *    
     */
    router.delete('/conversation/:id/:userId', authenticated_jwt , deleteUserHandler) 

    /**
     * @api {post} conversation/:id/add/:userId AddUserToConv
     * @apiDescription Add user to existing conversation
     * @apiPermission Authorized
     * @apiVersion 1.0.0
     * @apiName AddUserToConv
     * @apiGroup Conversation
     *
     * @apiExample Example usage:
     * curl -X POST   http://localhost:8000/api/conversation/5b1f69b20c92e72b91675ccf/add/5bc8d075369fcd4978d55196   -H 'Authorization: JWT ...' 
     *
     * @apiParam {String} id ConversationID.
     * @apiParam {String} userId UserID.
     * 
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response
     * @apiSuccess {Object}   response.conversation  Info about conv that can be sended as notification to added user
     *   
     *
     * @apiSuccessExample {json} Successful-Request:
     *     HTTP/1.1 201 CREATED
     *       {
     *           "status": "ok",
     *           "response": {
     *               "conversation": {
     *                   "title": "Yoba222",
     *                   "id": "5b4b49828a9134036cf47d15",
     *                   "lastMsg": {
     *                       "conversationId": "5b4b49828a9134036cf47d15",
     *                       "body": "HELOL",
     *                       "author": {
     *                           "displayName": "Yoba111",
     *                           "id": "5b3349f7ac63a90c7779489d"
     *                       },
     *                       "createdAt": "2018-07-15T13:17:54.272Z",
     *                       "updatedAt": "2018-07-15T13:17:54.272Z"
     *                   }
     *               }
     *           }
     *       }
     * @apiError (Error 422) UnprocessableEntity  You are not a participant of ${conversationId} conversation
     * @apiError (Error 422) UnprocessableEntity_  User ${userId} already in ${conversationId} conversation
     * @apiError (Error 422) UnprocessableEntity__ User ID not found 
     * @apiError (Error 422) UnprocessableEntity___  Invalid conversation ID
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 422 Unprocessable Entity
     *     {
     *           "status": "error",
     *           "error": {       
     *                 "name": "UnprocessableEntityError",
     *                   "message": "User ID not found ",
     *           }
     *       } 
     *    
     */
    router.post('/conversation/:id/add/:userId', authenticated_jwt , addUserToConvHandler) 
};