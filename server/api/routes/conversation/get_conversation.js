const mongoose = require('mongoose');
const { Message, Conversation } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);
const MAX_MSGS_COUNT_LIMIT = 100;//TODO put in config

//return all messages from conv
module.exports = getConversationHandler = async (ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(Object.assign(ctx.params, ctx.query))}`);

    const { conversationId, offset, count } = await parseReq(ctx)
        .catch(err => {
            ctx.throw(err.code, err.message)
        })

    const messages = await Message
        .find({ conversationId })
        .skip(offset)
        .limit(count)
        .sort('-createdAt')
        .populate([{ //fetch displayName from User model
            path: 'author',
            select: 'displayName'
        }])
    const totalMessages = await Message
        .find({ conversationId })
        .count()

    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: { total: totalMessages,offset, count, messages }
    };
}

async function parseReq(ctx) {
    const { offset: offsetStr, count: countStr } = ctx.query;

    const offset = parseInt(offsetStr)
    const count = parseInt(countStr)

    const isGoodOffset = !Number.isNaN(offset)
    if (!isGoodOffset || offset < 0) {
        throw { code: 400, message: 'Invalid offset' }
    }

    const isGoodCount = !Number.isNaN(count)
    if (!isGoodCount || count < 0 || count > MAX_MSGS_COUNT_LIMIT) {
        throw { code: 400, message: 'Invalid count' }
    }

    const { id: conversationId } = ctx.params;
    const isValidConvId = mongoose.Types.ObjectId.isValid(conversationId)
    if (!isValidConvId) {
        throw { code: 400, message: 'Invalid conversation id' }
    }
    //we verify that the user is in this group
    const authorId = ctx.state.user.id;
    const convExist = await Conversation.find({
        _id: conversationId,
        participants: {
            $all: [authorId]
        },
    })
    if (convExist.length === 0) {
        throw { code: 422, message: 'Unexisting conversation ID' }
    }

    return { conversationId, count, offset }
}