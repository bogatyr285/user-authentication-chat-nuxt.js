const { Conversation } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);
// return information about conversation. whole Conversation schema
module.exports = getConversationInfoHandler = async (ctx, next) => {
    const { id: conversationId } = ctx.params;
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(Object.assign(ctx.params, ctx.request.body))}`);

    const conversation = await Conversation
        .findById(conversationId)
        .populate({ //fetch displayName from User model
            path: 'participants',
            select: 'displayName'
        })
        .populate({
            path: 'master',
            select: 'displayName'
        })
    if (!conversation) {
        throw { code: 422, message: 'Unexisting conversation ID' }
    }
    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: { conversation }
    };
}