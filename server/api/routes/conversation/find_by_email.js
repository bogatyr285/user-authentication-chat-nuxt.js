const { User } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);

module.exports = findByEmailHandler = async(ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(Object.assign(ctx.params,ctx.request.body))}`);
    const { email } = ctx.params
    let user = await User.findOne({ email });
    log.info(`#${ctx.state.reqId}. Found user: ${user}`);
    return ctx.body = {
        status: 'ok',
        response: { user }
    };
}