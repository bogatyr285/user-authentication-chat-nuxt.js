const mongoose = require('mongoose');
const { Conversation, Message, User } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module)

//create new converstation with requested user
module.exports = newConversationHandler = async(ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(Object.assign(ctx.params,ctx.request.body))}`)

    const { senderId, recipientId, convTitle, firstMessage } = await parseReq(ctx)
        .catch(err => {
            ctx.throw(err.code, err.message)
        })

    const newConversation = await Conversation.create({
        participants: [senderId, recipientId],
        master:senderId,
        title: convTitle
    })

    const newMsg = await Message.create({
        conversationId: newConversation.id,
        body: firstMessage,
        author: senderId
    })
    const message = await Message.findById(newMsg.id) //append to message author's info
        .populate({ //fetch displayName from User model
            path: 'author',
            select: 'displayName'
        })
    log.info(`#${ctx.state.reqId}. Conv created. id:${newConversation.id}`)
    ctx.status = 201 //201 "created"
    return ctx.body = {
        status: 'ok',
        response: {
            conversation: {
                title: newConversation.title,
                id: newConversation.id,
                lastMsg:message
            }
        }
    }
}

async function parseReq(ctx) {
    const senderId = ctx.state.user.id;
    const { recipient: recipientId } = ctx.params;
    const isValidRecipientId = mongoose.Types.ObjectId.isValid(recipientId)
    if (!isValidRecipientId || senderId === recipientId) { 
        throw { code: 422, message: `Please choose a valid recipient for your message` } //422 "unprocessable entity"
    }
    const existingRecipient = await User.findById(recipientId)
    if (!existingRecipient) {
        throw { code: 422, message: `Please choose a valid recipient for your message` }
    }

    const { message: firstMessage } = ctx.request.body;
    if (!firstMessage) { //TODO mb insert escape charachers process?
        throw { code: 422, message: `Please enter a message` } //422 "unprocessable entity"
    }
    const { title: convTitle } = ctx.request.body;
    if (!convTitle) { //TODO mb insert escape charachers process?
        throw { code: 422, message: `Please enter a title` } //422 "unprocessable entity"
    }

    const alredyCreatedConv = await Conversation.find({ //check if conversation with user was already created
        participants: {
            $all: [senderId, recipientId],
            $size: 2
        }
    })
    if (alredyCreatedConv.length !== 0) {
        throw { code: 422, message: `You already have a conversation with this user` }
    }

    return { senderId, recipientId, convTitle, firstMessage }
}