const mongoose = require('mongoose');
const { Message, Conversation } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);

module.exports = sendMsgHandlerHandler = async(ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(Object.assign(ctx.params,ctx.request.body))}`);

    const { conversationId, authorId, msgBody } = await parseReq(ctx)
        .catch(err => {
            ctx.throw(err.code, err.message)
        })

    let newMsg = await Message.create({
        conversationId,
        body: msgBody,
        author: authorId,
    })
    const message = await Message.findById(newMsg.id) //append to message author's info
        .populate({ //fetch displayName from User model
            path: 'author',
            select: 'displayName'
        })
    log.info(`#${ctx.state.reqId}. Msg sended. id:${message.id}`);
    return ctx.body = {
        status: 'ok',
        response: { message }

    };
}

async function parseReq(ctx) {
    const { id: conversationId } = ctx.params;
    const isValidConvId = mongoose.Types.ObjectId.isValid(conversationId)
    if (!isValidConvId) {
        throw { code: 400, message: 'Invalid conversation id' }
    }
    //we verify that the user is in this group
    const authorId = ctx.state.user.id;
    const convExist = await Conversation.find({
        _id: conversationId,
        participants: {
            $all: [authorId]
        },
    })

    if (convExist.length === 0) {
        throw { code: 422, message: 'Unexisting conversation ID' }
    }
    const { message: body } = ctx.request.body;
    if (!body) {
        throw { code: 400, message: `Unspecified 'message' field` }
    }
    return { conversationId, authorId, msgBody: body }
}