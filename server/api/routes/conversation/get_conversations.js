const { Conversation, Message } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);
// Only return one message from each conversation to display as snippet
module.exports = getConversationsSnippetsHandler = async(ctx, next) => {
    const userId = ctx.state.user.id;
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl}  Params:${JSON.stringify(Object.assign(ctx.params,ctx.request.body))}`);
    const conversations = await Conversation
        .find({ participants: userId })
        .sort(`-updatedAt`)

    let conversationsSnippets = [];
    for (const conversation of conversations) {
        const lastMessage = await Message
            .findOne({ conversationId: conversation.id })
            .sort(`-updatedAt`)  //get last message       
            .populate({ //fetch displayName from User model
                path: 'author',
                select: 'displayName'
            })
            .select('-conversationId') //exclude this field because we'll add conv in below

        conversationsSnippets.push({
            title: conversation.title,
            id: conversation.id,
            lastMsg: lastMessage
        });
    }   
    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = {
        status: 'ok',
        response: { total: conversations.length, conversations: conversationsSnippets }
    };
}