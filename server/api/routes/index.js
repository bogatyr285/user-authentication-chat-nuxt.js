const auth = require('./auth')
const conversation = require('./conversation')
const Router = require('koa-router');
const router = new Router({ prefix: '/api' });

auth(router);
conversation(router);

module.exports = router;

/**
 * 
 * const { authenticated } = require('./middlewares')
router
    .post('/secret', authenticated, secretDataHandler)

 */