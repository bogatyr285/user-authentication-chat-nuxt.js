const { User } = require('../../../db/models')
const Logger = require('../../../logger')
const log = Logger(module);

const singupHandler = async (ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.body)}`);

    const { displayName, password, email } = ctx.request.body;
    const user = new User({
        displayName,
        password,
        email
    });
    try {
        await user.save()
    } catch (error) {
        if (error.code === 11000) {
            ctx.throw(409, 'Email already exist') //409 "conflict"
        }
        if (error.name === 'ValidationError') {
            ctx.throw(422, error)
        }
        log.error(`Smth bad with DB:${error.name} ${error.message} ${error.stack}`)
        ctx.throw(5000, error) //500 "internal server error"    
    }
    ctx.status = 201; //201 "created"
    log.info(`#${ctx.state.reqId}. Successful register. User: ${user.email}`)

    return ctx.body = {
        status: 'ok',
        response: { user: user.toJSON() }
    };
}

module.exports = singupHandler