const passport = require('koa-passport');
const jwt = require('jsonwebtoken');
const { jwt: jwtCfg } = require('../../../config')

const Logger = require('../../../logger')
const log = Logger(module);

const loginHandler = async(ctx, next) => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl} Params:${JSON.stringify(ctx.request.body)}`);
    await passport.authenticate('local', function(err, user) {
        if (!user) {
            ctx.throw(401, err) //401 "unauthorized"
        } else {
            //payload - информация которую мы храним в токене и можем из него получать
            const payload = {
                id: user.id,
                displayName: user.displayName,
                email: user.email
            };
            const token = jwt.sign(payload, jwtCfg.secret, { issuer: jwtCfg.issuer, expiresIn: jwtCfg.accessTokenExpiration }); //sign jwt token JWT
            ctx.session.authUser = { displayName: user.displayName, email: user.email, id: user.id, token } //Will be avaible in nuxtServerInit

            ctx.body = {
                status: 'ok',
                response: { user: ctx.session.authUser }
            };
            log.info(`#${ctx.state.reqId}. User logged in ${JSON.stringify(payload)}`);
            return ctx.login(user)
        }
    })(ctx, next);
}

module.exports = loginHandler