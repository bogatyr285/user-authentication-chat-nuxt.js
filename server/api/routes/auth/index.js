const loginHandler = require('./login')
const logoutHandler = require('./logout')
const singupHandler = require('./singup')
module.exports = (router) => {
    /**
     * @api {post} /api/auth/singup Singup
     * @apiPermission none
     * @apiVersion 1.0.0
     * @apiName Singup
     * @apiGroup Auth
     *
     * @apiExample Example usage:
     * curl -X POST   http://localhost:8000/api/auth/singup -H 'Content-Type: application/x-www-form-urlencoded' -d 'email=qq%40qq.qq&password=qqq&displayName=User1'
     *
     * @apiParam {String} email Email.
     * @apiParam {String} password Password.
     * @apiParam {String} displayName User's display name.
     *
     * @apiSuccess {String}   status            Request status. 'ok' or 'error'
     * @apiSuccess {Object}   response          Response    
     * @apiSuccess {Object}   response.user     Info about user
     *   
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *           "status": "ok",
     *           "response": {
     *               "user": {
     *                   "id": "5b1f6bf97b96192b9e1cc4a3",
     *                   "displayName": "User1",
     *                   "email": "qq@qq.qq",
     *                   "createdAt": "2018-06-12T06:45:13.237Z",
     *                   "updatedAt": "2018-06-12T06:45:13.237Z"
     *               },                  
     *           }
     *       }   
     *
     * @apiError (Error 409)    Conflict             Email already exist
     * @apiError (Error 500)    InternalServerError  Communication with DB error
     *
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 409 Conflict
     *     {
     *           "status": "error",
     *           "error": {       
     *                "name": "ConflictError",
     *                "message": "Email already exist", 
     *           }
     *       } 
     * 
     */
    router.post('/auth/singup', singupHandler)
        /**
         * @api {post} /api/auth/login Login
         * @apiPermission none
         * @apiVersion 1.0.0
         * @apiName Login
         * @apiGroup Auth
         *
         * @apiExample Example usage:
         * curl -X POST   http://localhost:8000/api/auth/login -H 'Content-Type: application/x-www-form-urlencoded' -d 'email=qq%40qq.qq&password=qqq'
         *
         * @apiParam {String} email Email.
         * @apiParam {String} password Password.
         *
         * @apiSuccess {String}   status            Request status. 'ok' or 'error'
         * @apiSuccess {Object}   response          Response    
         * @apiSuccess {Object}   response.user     Info about user
         * @apiSuccess {String}   response.token    JWT token
         *   
         *
         * @apiSuccessExample {json} Success-Response:
         *     HTTP/1.1 200 OK
         *     {
         *           "status": "ok",
         *           "response": {
         *               "user": {
         *                   "id": "5b1f6bf97b96192b9e1cc4a3",
         *                   "displayName": "1234",
         *                   "email": "qq@qq.qq",
         *                   "createdAt": "2018-06-12T06:45:13.237Z",
         *                   "updatedAt": "2018-06-12T06:45:13.237Z"
         *               },
         *               "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViMWY2YmY5N2I5NjE5MmI5ZTFjYzRhMyIsImRpc3BsYXlOYW1lIjoiMTIzNCIsImVtYWlsIjoicXFAcXEucXEiLCJpYXQiOjE1MjkyMjU5MjUsImV4cCI6MTYxNTYyNTkyNSwiaXNzIjoiQXdlc29tZVNlcnZpY2UifQ.GzhbQyWFl6avGmZ5hWvvaqcm0zr0SBHXApgMC5YtGtY"
         *           }
         *       }   
         *
         * @apiError (Error 401) UnauthorizedError You're not authenticated
         *
         * @apiErrorExample {json} Error-Response:
         *     HTTP/1.1 401 Unauthorized
         *     {
         *           "status": "error",
         *           "error": {       
         *                "name": "UnauthorizedError",
         *                "message": "Email does not exist", 
         *           }
         *       } 
         * 
         */
    router.post('/auth/login', loginHandler)
        /**
         * @api {post} /api/auth/logout Logout
         * @apiPermission none
         * @apiVersion 1.0.0
         * @apiName Logout
         * @apiGroup Auth
         *
         * @apiExample Example usage:
         * curl -X POST   http://localhost:8000/api/auth/logout 
         *     
         *
         * @apiSuccess {String}   status            Request status. 'ok' or 'error'
         *   
         *
         * @apiSuccessExample {json} Success-Response:
         *     HTTP/1.1 200 OK
         *       {
         *           "status": "ok"
         *       }  
         */
    router.post('/auth/logout', logoutHandler)
};