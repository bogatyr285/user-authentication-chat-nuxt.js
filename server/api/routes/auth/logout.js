const Logger = require('../../../logger')
const log = Logger(module);

const logoutHandler = async ctx => {
    log.info(`#${ctx.state.reqId}. IP: ${ctx.request.ip} ${ctx.method} ${ctx.originalUrl}`);
    ctx.session.authUser = null
    ctx.logout()
    log.info(`#${ctx.state.reqId}. ok`);
    return ctx.body = { status: 'ok' }
}

module.exports = logoutHandler