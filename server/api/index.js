const http = require('http')
const Koa = require('koa')
const KoaBody = require('koa-body')
const router = require('./routes')
const session = require("koa-session");
const redisStore = require("koa-redis-session");
const { redis: redisCfg } = require('../config')
const passport = require('./auth');

const { setRequestId, errorsHandler, } = require('./middlewares')
const Logger = require('../logger')
const log = Logger(module);
const Nuxt = require('./nuxt');
const SocketIO = require('./socket')

module.exports = async (host, port) => {
    const nuxt = await Nuxt()

    const app = new Koa();
    // manually create http server 
    const server = http.createServer(app.callback())
    // create Socket.io app
    SocketIO(server)
    app.keys = [`super-secret-key_}`] //for session middleware

    app
        .use(errorsHandler)
        .use(session({
            store: new redisStore({
                port: redisCfg.port,
                host: redisCfg.host,
                family: 4,// 4 (IPv4) or 6 (IPv6)
                db: 0,
                onError: (e) => log.error(`Redis was broken: ${e.name} ${e.message}`),
            }),
        }, app))
        //pasport init
        .use(passport.initialize())
        .use(passport.session())
        // body parser
        .use(KoaBody())
        .use(setRequestId)
        //routers
        .use(router.routes())
        .use(router.allowedMethods())
        //nuxt middleware
        .use(ctx => {
            ctx.status = 200 // koa defaults to 404 when it sees that status is unset
            ctx.req.session = ctx.session //  Данные поля будут доступны в nuxtServerInit
            //    console.log('session main koa', ctx.req.session)
            ctx.req.state = ctx.state //  nuxtServerInit
            return new Promise((resolve, reject) => {
                ctx.res.on('close', resolve)
                ctx.res.on('finish', resolve)
                nuxt.render(ctx.req, ctx.res, promise => {
                    // nuxt.render passes a rejected promise into callback on error.
                    promise.then(resolve).catch(reject)
                })
            })
        })
    server.listen({
        host,
        port,
        exclusive: true
    }, () => {
        log.info(`Server listening on http://${host}:${port}`)
    })
}