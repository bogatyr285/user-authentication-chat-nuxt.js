const randomstring = require('randomstring')

const setReqId = async(ctx, next) => {
    ctx.state.reqId = randomstring.generate({
        length: 8,
        charset: 'alphanumeric',
        readable: true,
        capitalization: 'lowercase'
    });
    await next();
}
module.exports = setReqId