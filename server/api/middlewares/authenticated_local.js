const authenticated_jwt = async(ctx, next) => {
    if (ctx.isAuthenticated()) {
        await next();
    } else {
        ctx.throw(401, `You're not authenticated`)
    }
}
module.exports = authenticated_jwt