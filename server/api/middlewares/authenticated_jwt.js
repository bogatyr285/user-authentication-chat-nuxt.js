const passport = require('koa-passport')

const authenticated_jwt = async(ctx, next) => {
    await passport.authenticate('jwt', { session: false }, async(err, user) => {
        if (!user || err) {
            ctx.throw(401, `You're not authenticated` || err.message)
        }
        ctx.state.user = user; //!!! now info about authenticated user avaible in ctx.state.user
        await next()
    })(ctx, next);
}
module.exports = authenticated_jwt