const Logger = require('../../logger');
const log = Logger(module);

const errorHandler = async(ctx, next) => {
    try {
        await next();
    } catch (err) {
        let msg = {
            name: err.name,
            message: err.message,        
            errors: err.errors
        };
        const statusCode = err.statusCode || err.status;
        if (statusCode) {
            log.warn(`#${ctx.state.reqId}. Request rejected. Reason: ${JSON.stringify(msg, null, 2)}`);
        } else {
            if ((err instanceof Error || err.stack) && process.env.NODE_ENV != 'production') { //!!!FOR DEBUG PURPOSES add stack trace
                msg = Object.assign(msg, { stack: err.stack });
            }
            log.error(`#${ctx.state.reqId}. ${err.type?err.type:'Error occured'}.Reason: ${JSON.stringify(msg, null, 2)}`);
        }

        ctx.status = statusCode || 500;

        return ctx.body = {
            status: 'error',
            error: msg
        };
    }
}

module.exports = errorHandler