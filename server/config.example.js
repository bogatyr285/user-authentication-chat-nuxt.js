const server = {
    host: process.env.HOST || '0.0.0.0', // if u want to run nuxt inside docker dont change this value to 'localhost' or '127.0.0.1'
    port: process.env.PORT || 8080,
};
const mongo = {
    host: process.env.MONGO_HOST || '127.0.0.1',
    port: process.env.MONGO_PORT || 27017,
    collectionName: process.env.MONGO_DB_NAME || 'chat',
    debug: true //will show all action that mongo do
};
const redis = {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: process.env.REDIS_PORT || 6379,
    password: process.env.REDIS_PASSWD || 'passwd'
};
const jwt = {
    secret: process.env.JWT_SECRET || `some_secretData_2W3D>@Yu5>bPc&M%#PCsVH)WX94bZh6u}`,
    issuer: process.env.JWT_ISSUER || 'AwesomeService',
    accessTokenExpiration: process.env.ACCESS_TOKEN_EXPIRATION || 86400000, //1d is ms
};

module.exports = { server, mongo, jwt, redis }