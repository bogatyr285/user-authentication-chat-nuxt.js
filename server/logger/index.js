const path = require('path')
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf, prettyPrint } = format;

const getFilePath = m => m.filename.split(path.sep).slice(-2).join(path.sep);
const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${JSON.stringify(info.message,null,2)}`;
});

module.exports = (_module) => {
    return createLogger({
        format: combine(label({ label: getFilePath(_module) }), timestamp(), format.colorize(), prettyPrint(), myFormat),
        transports: [
            new transports.Console({
                level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
            }),
        ],
    })
}