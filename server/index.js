const apiStart = require('./api');
const dbStart = require('./db');
const Logger = require('./logger')
const log = Logger(module);

const { server: cfg } = require('./config')
process.on('uncaughtException', error => {
    log.error('Unhandled exeption:')
    log.error(`${error.name} ${error.message} ${error.stack}`);
});
process.on('unhandledRejection', error => {
    log.error('Unhandled rejection:')
    log.error(`${error.name} ${error.message} ${error.stack}`);
});

(async() => {
    try {
        await dbStart();
        await apiStart(cfg.host, cfg.port);
    } catch (error) {
        console.log('Something went wrong:\n', error)
        process.exit(1)
    }
})();